FROM openjdk:8
ENV JAVA_OPTS="-Xms256m -Xmx1024m -Dspring.config.location=config/application.yml"
COPY config /app/inventory-svc/config
COPY target/lib /app/inventory-svc/lib
COPY target/inventory-svc-1.0.jar /app/inventory-svc/inventory-svc-1.0.jar

WORKDIR /app/inventory-svc/
