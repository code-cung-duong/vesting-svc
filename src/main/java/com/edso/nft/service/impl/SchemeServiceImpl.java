package com.edso.nft.service.impl;

import com.edso.nft.common.CollectionNameDefs;
import com.edso.nft.common.DBConfigKey;
import com.edso.nft.common.db.MongoDbOnlineSyncActions;
import com.edso.nft.dto.messagekafka.ProcessCreateSchemeMessage;
import com.edso.nft.dto.messagekafka.SchemePlan;
import com.edso.nft.enumtype.VestingStatus;
import com.edso.nft.service.BaseService;
import com.edso.nft.service.SchemeService;
import com.mongodb.client.model.Filters;
import lombok.extern.log4j.Log4j2;
import org.bson.Document;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
@Log4j2
public class SchemeServiceImpl extends BaseService implements SchemeService {
    private final  MongoDbOnlineSyncActions db;

    public SchemeServiceImpl(MongoDbOnlineSyncActions db) {
        this.db = db;
    }

    @Override
    @Transactional
    public void processCreateScheme(ProcessCreateSchemeMessage evt) {
        log.info("- - - - - - - - - - Process create scheme");
        Document old = db.findOne(CollectionNameDefs.COLL_VESTING, Filters.eq(DBConfigKey.TXN_HASH, evt.getTxnHash()));
        if (Objects.nonNull(old)) {
            log.info(" - - - - - - - - - - Ignore process create scheme , txnHash = {}", evt.getTxnHash());
            return;
        }
        Document scheme = new Document();
        scheme.append(DBConfigKey.SCHEME_NAME, evt.getSchemeName());
        scheme.append(DBConfigKey.CRYPTO_ASSET_SYMBOL, evt.getCryptoAssetSymbol());
        scheme.append(DBConfigKey.DURATION_TIME, evt.getDurationTime());
        scheme.append(DBConfigKey.AFTER_START_TIME, evt.getAfterStartTime());
        scheme.append(DBConfigKey.AFTER_START_TYPE, evt.getAfterStartType());
        scheme.append(DBConfigKey.STATUS, VestingStatus.PROCESSING_TRANSACTION.getCode());
        scheme.append(DBConfigKey.TXN_HASH, evt.getTxnHash());
        scheme.append(DBConfigKey.CREATED_AT, System.currentTimeMillis());
        scheme.append(DBConfigKey.UPDATED_AT, System.currentTimeMillis());
        scheme.append(DBConfigKey.IS_DELETE, false);
        db.insertOne(CollectionNameDefs.COLL_SCHEME, scheme);
        Document finalScheme = db.findOne(CollectionNameDefs.COLL_VESTING, Filters.eq(DBConfigKey.TXN_HASH, evt.getTxnHash()));

        List<Document> plans = new ArrayList<>();
        evt.getSchemePlan().forEach(schemePlan -> {
            Document plan = new Document();
            plan.append(DBConfigKey.PERIOD, schemePlan.getPeriod());
            plan.append(DBConfigKey.PERCENT_TOKEN_RELEASE, schemePlan.getPercentTokenRelease());
            plan.append(DBConfigKey.IS_LOCKED, schemePlan.getIsLocked());
            plan.append(DBConfigKey.SCHEME_ID, finalScheme.get(DBConfigKey.MONGO_ID));
            plans.add(plan);
        });
        db.insertMany(CollectionNameDefs.COLL_SCHEME_PLAN, plans);
        log.info("- - - - - - - - - - Successful process create scheme");
    }
}
