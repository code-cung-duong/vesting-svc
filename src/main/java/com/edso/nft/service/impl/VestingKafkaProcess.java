package com.edso.nft.service.impl;

import com.edso.nft.dto.messagekafka.*;
import com.edso.nft.service.SchemeService;
import com.edso.nft.service.VestingService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
@Data
public class VestingKafkaProcess {

    private final Logger logger = LoggerFactory.getLogger(getClass());
    private final Gson gson = new GsonBuilder()
            .setDateFormat("dd-MM-yyyy hh:mm:ss").create();
    private final VestingService vestingService;
    private final SchemeService schemeService;


    public void process(String event) throws Exception {
        try {
            JsonObject obj = gson.fromJson(event, JsonObject.class);
            if (obj == null) {
                return;
            }
            processEventMessage(obj);
        } catch (Throwable ex) {
            logger.error("Ex: ", ex);
        }
    }

    private void processEventMessage(JsonObject object) {
        if (object.has("ProcessActiveVestingMessage")) {
            JsonElement element = object.get("ProcessActiveVestingMessage");
            if (element != null && element.isJsonArray()) {
                for (JsonElement e : element.getAsJsonArray()) {
                    JsonObject o = e.getAsJsonObject();
                    if (o != null) {
                        ProcessActiveVestingMessage evt = gson.fromJson(o, ProcessActiveVestingMessage.class);
                        vestingService.processActiveVesting(evt);
                    }
                }
            }
        }

        if (object.has("ActiveVestingMessage")) {
            JsonElement element = object.get("ActiveVestingMessage");
            if (element != null && element.isJsonArray()) {
                for (JsonElement e : element.getAsJsonArray()) {
                    JsonObject o = e.getAsJsonObject();
                    if (o != null) {
                        ActiveVestingMessage evt = gson.fromJson(o, ActiveVestingMessage.class);
                        vestingService.processActiveVesting(evt);
                    }
                }
            }
        }

        if (object.has("ProcessCreateVestingMessage")) {
            JsonElement element = object.get("ProcessCreateVestingMessage");
            if (element != null && element.isJsonArray()) {
                for (JsonElement e : element.getAsJsonArray()) {
                    JsonObject o = e.getAsJsonObject();
                    if (o != null) {
                        ProcessCreateVestingMessage evt = gson.fromJson(o, ProcessCreateVestingMessage.class);
                        vestingService.processCreateVesting(evt);
                    }
                }
            }
        }

        if (object.has("CreateVestingMessage")) {
            JsonElement element = object.get("CreateVestingMessage");
            if (element != null && element.isJsonArray()) {
                for (JsonElement e : element.getAsJsonArray()) {
                    JsonObject o = e.getAsJsonObject();
                    if (o != null) {
                        CreateVestingMessage evt = gson.fromJson(o, CreateVestingMessage.class);
                        vestingService.createVesting(evt);
                    }
                }
            }
        }

        if (object.has("ProcessCreateSchemeMessage")) {
            JsonElement element = object.get("ProcessCreateSchemeMessage");
            ProcessCreateSchemeMessage evt = null;
            if (element != null && element.isJsonArray()) {
                for (JsonElement e : element.getAsJsonArray()) {
                    JsonObject o = e.getAsJsonObject();
                    if (o != null) {
                        evt = gson.fromJson(o, ProcessCreateSchemeMessage.class);
                    }
                }
            }
//            JsonObject obj = gson.fromJson(element.toString(), JsonObject.class);
//            if (Objects.nonNull(evt)) {
//                JsonElement element2 = element.
//                if (element2 != null && element2.isJsonArray()) {
//                    for (JsonElement e : element2.getAsJsonArray()) {
//                        JsonObject o = e.getAsJsonObject();
//                        if (o != null) {
//                            SchemePlan evt2 = gson.fromJson(o, SchemePlan.class);
//                            evt.getSchemePlan().add(evt2);
//                        }
//                    }
//                }
//            }
            schemeService.processCreateScheme(evt);
        }


    }

}
