package com.edso.nft.service.impl;


import com.edso.nft.common.CollectionNameDefs;
import com.edso.nft.common.DBConfigKey;
import com.edso.nft.common.GetArrayResponse;
import com.edso.nft.common.db.MongoDbOnlineSyncActions;
import com.edso.nft.common.info.PagingInfo;
import com.edso.nft.dto.messagekafka.ActiveVestingMessage;
import com.edso.nft.dto.messagekafka.CreateVestingMessage;
import com.edso.nft.dto.messagekafka.ProcessActiveVestingMessage;
import com.edso.nft.dto.messagekafka.ProcessCreateVestingMessage;
import com.edso.nft.dto.response.VestingResponseDTO;
import com.edso.nft.entity.Vesting;
import com.edso.nft.enumtype.VestingStatus;
import com.edso.nft.mapper.VestingMapper;
import com.edso.nft.service.BaseService;
import com.edso.nft.service.VestingService;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Sorts;
import com.mongodb.client.model.Updates;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang.StringUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.bson.types.ObjectId;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Pattern;

@Service
@Log4j2
public class VestingServiceImpl extends BaseService implements VestingService {
    private final MongoDbOnlineSyncActions db;
    private final VestingMapper vestingMapper;

    public VestingServiceImpl(MongoDbOnlineSyncActions db, VestingMapper vestingMapper) {
        this.db = db;
        this.vestingMapper = vestingMapper;
    }

    @Override
    public void processActiveVesting(ProcessActiveVestingMessage evt) {
        log.info("- - - - - - - - - - Process active vesting");
        List<Bson> bs = new ArrayList<>();
        bs.add(Filters.eq(DBConfigKey.MONGO_ID, evt.getId()));
        bs.add(Filters.eq(DBConfigKey.WALLET_ADDRESS, evt.getWalletAddress().toLowerCase()));
        Document vesting = db.findOne(CollectionNameDefs.COLL_VESTING, buildCondition(bs));
        if (Objects.isNull(vesting)) {
            log.info("- - - - - - - - - - Error! Vesting not found");
            return;
        }
        bs.clear();
        bs.add(Filters.eq(DBConfigKey.WALLET_ADDRESS, evt.getWalletAddress().toLowerCase()));
        bs.add(Filters.eq(DBConfigKey.TXN_HASH, evt.getTxnHash()));
        Document c = db.findOne(CollectionNameDefs.COLL_VESTING, buildCondition(bs));
        if (Objects.nonNull(c)) {
            log.info(" - - - - - - - - - - Ignore process create vesting ");
            return;
        }
        bs.clear();
        bs.add(Updates.set(DBConfigKey.STATUS, VestingStatus.PROCESSING_TRANSACTION.getCode()));
        bs.add(Updates.set(DBConfigKey.UPDATED_AT, System.currentTimeMillis()));
        bs.add(Updates.set(DBConfigKey.TXN_HASH, evt.getTxnHash()));
        db.update(CollectionNameDefs.COLL_VESTING, Filters.eq(DBConfigKey.MONGO_ID, evt.getId()), buildCondition(bs));
        log.info("- - - - - - - - - - Successful Process active vesting");
    }

    @Override
    public void activeVesting(ActiveVestingMessage evt) {
        log.info("- - - - - - - - - - Active vesting");
        List<Bson> bs = new ArrayList<>();
        bs.add(Filters.eq(DBConfigKey.MONGO_ID, evt.getId()));
        bs.add(Filters.eq(DBConfigKey.TXN_HASH, evt.getTxnHash()));
        Bson condition = buildCondition(bs);
        Document vesting = db.findOne(CollectionNameDefs.COLL_VESTING, condition);
        if (Objects.isNull(vesting)) {
            log.info("- - - - - - - - - - Error! Vesting not found");
            return;
        }
        if (!vesting.get(DBConfigKey.STATUS).equals(VestingStatus.PROCESSING_TRANSACTION.getCode())) {
            log.info("- - - - - - - - - - Error! Vesting status is not processing transaction");
            return;
        }
        bs.clear();
        bs.add(Updates.set(DBConfigKey.STATUS, VestingStatus.ACTIVE.getCode()));
        bs.add(Updates.set(DBConfigKey.UPDATED_AT, System.currentTimeMillis()));
        db.update(CollectionNameDefs.COLL_VESTING, condition, buildCondition(bs));
        log.info("- - - - - - - - - - Successful Active vesting");
    }

    @Override
    public void processCreateVesting(ProcessCreateVestingMessage evt) {
        log.info("- - - - - - - - - - Process create vesting");
        if (Objects.isNull(evt.getWalletAddress())
                || StringUtils.isBlank(evt.getWalletAddress())) {
            log.info(" - - - - - - - - - - Invalid wallet address");
            return;
        }
        ObjectId sid= new ObjectId(evt.getSchemeId());
        Document scheme = db.findOne(CollectionNameDefs.COLL_SCHEME, Filters.eq(DBConfigKey.MONGO_ID, sid));
        if (Objects.isNull(scheme)) {
            log.info(" - - - - - - - - - - Scheme ID not found: {}", evt.getSchemeId());
            return;
        }
        Document old = db.findOne(CollectionNameDefs.COLL_VESTING, Filters.eq(DBConfigKey.TXN_HASH, evt.getTxnHash()));
        if (Objects.nonNull(old)) {
            log.info(" - - - - - - - - - - Ignore process create vesting , txnHash = {}", evt.getTxnHash());
            return;
        }
        Document vesting = new Document();
        vesting.append(DBConfigKey.TXN_HASH, evt.getTxnHash());
        vesting.append(DBConfigKey.IS_HAS_TOKEN, evt.getIsHasToken());
        vesting.append(DBConfigKey.SCHEME, evt.getSchemeId());
        vesting.append(DBConfigKey.WALLET_ADDRESS, evt.getWalletAddress().toLowerCase());
        vesting.append(DBConfigKey.TOTAL_AMOUNT_VESTING, evt.getAmount());
        vesting.append(DBConfigKey.CURRENT_AMOUNT_VESTING, 0);
        vesting.append(DBConfigKey.CLAIMED_TOTAL_AMOUNT, 0);
        vesting.append(DBConfigKey.STATUS, VestingStatus.PROCESSING_TRANSACTION.getCode());

        vesting.append(DBConfigKey.CREATED_AT, System.currentTimeMillis());
        vesting.append(DBConfigKey.UPDATED_AT, System.currentTimeMillis());
        vesting.append(DBConfigKey.IS_DELETE, false);
        db.insertOne(CollectionNameDefs.COLL_VESTING, vesting);
        log.info("- - - - - - - - - - Successful Process create vesting");
    }

    @Override
    public void createVesting(CreateVestingMessage evt) {
        log.info("- - - - - - - - - - Create vesting");
        List<Bson> bs = new ArrayList<>();
        bs.add(Filters.eq(DBConfigKey.TXN_HASH, evt.getTxnHash()));
        bs.add(Filters.eq(DBConfigKey.WALLET_ADDRESS, evt.getWalletAddress().toLowerCase()));
        Document vesting = db.findOne(CollectionNameDefs.COLL_VESTING, buildCondition(bs));
        if (Objects.isNull(vesting)) {
            log.info("- - - - - - - - - - Error! Vesting not found");
            return;
        }
        if (!vesting.get(DBConfigKey.STATUS).equals(VestingStatus.PROCESSING_TRANSACTION.getCode())) {
            log.info("- - - - - - - - - - Error! Vesting status is not processing transaction");
            return;
        }
        Bson condition = buildCondition(bs);
        bs.clear();
        if (evt.getIsHasToken()) {
            bs.add(Updates.set(DBConfigKey.STATUS, VestingStatus.ACTIVE.getCode()));
            bs.add(Updates.set(DBConfigKey.STARTED_TIME, System.currentTimeMillis()));
            /// FIXME: 1/17/2022 not fix
            bs.add(Updates.set(DBConfigKey.ENDED_TIME, VestingStatus.ACTIVE.getCode()));
        } else {
            bs.add(Updates.set(DBConfigKey.STATUS, VestingStatus.INACTIVE.getCode()));
        }
        db.update(CollectionNameDefs.COLL_VESTING, condition, buildCondition(bs), true);
        log.info("- - - - - - - - - - Successful Create vesting");
    }

    @Override
    public GetArrayResponse<VestingResponseDTO> getListVesting(HttpServletRequest request, String walletAddress, String symbolToken, Integer status, Pageable pageable) {
        GetArrayResponse<VestingResponseDTO> response = new GetArrayResponse<>();
        List<VestingResponseDTO> vestingResponseDTOS = new ArrayList<>();

        try {
            PagingInfo pagingInfo = PagingInfo.parse(pageable.getPageNumber(), pageable.getPageSize());
            List<Bson> condition = new ArrayList<>();
            if (walletAddress != null) {
                condition.add(Filters.regex(DBConfigKey.WALLET_ADDRESS,  ".*" + Pattern.quote(walletAddress.toLowerCase()) + ".*", "i"));
            }
            if (symbolToken != null) {
//                condition.add(Filters.regex(DBConfigKey.C,  ".*" + Pattern.quote(collectionName) + ".*", "i"));
            }
            if (status != null) {
                condition.add(Filters.eq(DBConfigKey.STATUS, status));
            }
            Bson sort = Sorts.descending(DBConfigKey.CREATED_AT);
            Bson cond = buildCondition(condition);
            long count = db.countAll(CollectionNameDefs.COLL_VESTING, cond);
            assert count != 0;
            List<Document> docs = db.findAllNotRemoveId(CollectionNameDefs.COLL_VESTING, cond, sort, pagingInfo.getStart(), pagingInfo.getLimit());
            docs.forEach(doc ->
                    vestingResponseDTOS.add(vestingMapper.toDTO(doc))
            );
            response.setTotal(count);
            response.setRows(vestingResponseDTOS);
            response.setSuccess();
            return response;
        } catch (Throwable ex) {
            logger.error("Lỗi lấy list !");
            return response;
        }
    }
}
