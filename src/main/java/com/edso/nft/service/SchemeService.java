package com.edso.nft.service;

import com.edso.nft.dto.messagekafka.ProcessCreateSchemeMessage;

import javax.servlet.http.HttpServletRequest;

public interface SchemeService {

    //SchemeCreateResponseDTO createScheme(HttpServletRequest request, SchemeCreateRequestDTO dto);

    void processCreateScheme(ProcessCreateSchemeMessage processCreateSchemeMessage);

}
