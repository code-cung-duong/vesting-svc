package com.edso.nft.service;

import com.edso.nft.common.GetArrayResponse;
import com.edso.nft.dto.messagekafka.ActiveVestingMessage;
import com.edso.nft.dto.messagekafka.CreateVestingMessage;
import com.edso.nft.dto.messagekafka.ProcessActiveVestingMessage;
import com.edso.nft.dto.messagekafka.ProcessCreateVestingMessage;
import com.edso.nft.dto.response.VestingResponseDTO;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletRequest;

public interface VestingService {

//    List<VestingResponseDTO> getListVesting(HttpServletRequest request,String walletAddress, String symbolToken, Integer status, Pageable pageable);
//
//    VestingResponseDTO getDetail(HttpServletRequest request, Long id);
//
//    VestingCreateResponseDTO createVesting(HttpServletRequest request, VestingCreateRequestDTO dto);
//
//    VestingResponseDTO activeVesting(HttpServletRequest request, Long id);
//
    //event kafka
    void processActiveVesting(ProcessActiveVestingMessage processActiveVestingMessage);

    void activeVesting(ActiveVestingMessage activeVestingMessage);

    void processCreateVesting(ProcessCreateVestingMessage processCreateVestingMessage);

    void createVesting(CreateVestingMessage createVestingMessage);

    GetArrayResponse<VestingResponseDTO> getListVesting(HttpServletRequest request, String walletAddress, String symbolToken, Integer status, Pageable pageable);

}
