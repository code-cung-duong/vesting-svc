package com.edso.nft.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.io.Serializable;

@Data
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class BaseModel implements Serializable {

    protected Long createdAt;

    protected Long updatedAt;

    protected Boolean isDeleted;

}
