package com.edso.nft.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Data
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class VestingSchemePlan extends BaseModel {

    private String vestingId;

    private String schemePlanId;

    private Integer status;
}
