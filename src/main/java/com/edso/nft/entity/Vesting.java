package com.edso.nft.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.math.BigDecimal;

@Data
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public class Vesting extends BaseModel {

    private String walletAddress;

    private String schemeId;

    private BigDecimal totalAmountVesting;

    private BigDecimal currentAmountVesting;

    private BigDecimal claimedTotalAmount;

    private Long startedTime;

    private Long endedTime;

    private Integer status;

    private String txnHash;

    private Boolean isHasToken;

    protected Boolean isDeleted;
}
