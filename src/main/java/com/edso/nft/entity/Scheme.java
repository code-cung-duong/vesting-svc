package com.edso.nft.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;


@Data
@SuperBuilder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Scheme extends BaseModel {

    private String schemeName;

    private Integer cryptoAssetId;

    private Integer durationTime;

    private Integer durationType;

    private Integer afterStartTime;

    private Integer afterStartType;

}
