package com.edso.nft.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor
@AllArgsConstructor
public class SchemeDTO {
    private String id;
    private String schemeName;
    private Integer cryptoAssetId;
    private Integer durationTime;
    private Integer durationType;
    private Integer afterStartTime;
    private Integer afterStartType;
    protected Boolean isDeleted;
}
