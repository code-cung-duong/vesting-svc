package com.edso.nft.dto.response;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
public class VestingCreateResponseDTO{
    private Long id;
}
