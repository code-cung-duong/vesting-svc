package com.edso.nft.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import java.math.BigDecimal;
import java.sql.Timestamp;

@Data
@RequiredArgsConstructor
public class VestingResponseDTO {
    private Long id;
    private String walletAddress;
    @JsonProperty("scheme")
    private SchemeDTO schemeDTO;
    private BigDecimal totalAmountVesting;
    private BigDecimal currentAmountVesting;
    private BigDecimal claimedTotalAmount;
    private Long startedTime;
    private Long endedTime;
    private Integer status;
    private String txnHash;
    private Boolean isHasToken;
    protected Boolean isDeleted;
}
