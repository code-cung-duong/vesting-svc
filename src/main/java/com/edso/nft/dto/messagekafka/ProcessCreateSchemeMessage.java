package com.edso.nft.dto.messagekafka;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
public class ProcessCreateSchemeMessage {
    private String txnHash;
    private String schemeName;
    private String cryptoAssetSymbol;
    private Integer durationTime;
    private Integer durationType;
    private Integer afterStartTime;
    private Integer afterStartType;
    private List<SchemePlan> schemePlan;
}
