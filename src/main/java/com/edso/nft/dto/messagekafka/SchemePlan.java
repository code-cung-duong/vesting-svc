package com.edso.nft.dto.messagekafka;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class SchemePlan {
    private Integer period;
    private Integer percentTokenRelease;
    private Boolean isLocked;
}
