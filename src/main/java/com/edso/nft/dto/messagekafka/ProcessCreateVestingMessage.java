package com.edso.nft.dto.messagekafka;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
public class ProcessCreateVestingMessage {
    private String walletAddress;
    private String schemeId;
    @NotNull(message = "Amount required not null!")
    private BigDecimal amount;
    private Boolean isHasToken;
    private String txnHash;
}
