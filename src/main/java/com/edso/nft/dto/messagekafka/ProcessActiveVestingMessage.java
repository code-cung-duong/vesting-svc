package com.edso.nft.dto.messagekafka;

import lombok.Data;

@Data
public class ProcessActiveVestingMessage {
    private String id;
    private String txnHash;
    private String walletAddress;
}
