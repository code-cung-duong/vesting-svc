package com.edso.nft.mapper;

import com.edso.nft.common.DBConfigKey;
import com.edso.nft.dto.response.SchemeDTO;
import com.edso.nft.dto.response.VestingResponseDTO;
import org.bson.Document;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

@Component
public class VestingMapper {
    public VestingResponseDTO toDTO(Document vesting) {
        VestingResponseDTO dto = new VestingResponseDTO();
        BeanUtils.copyProperties(vesting, dto);
        SchemeDTO schemeDTO = new SchemeDTO();
        BeanUtils.copyProperties(vesting.get(DBConfigKey.SCHEME), schemeDTO);
        dto.setSchemeDTO(schemeDTO);
        return dto;
    }
}
