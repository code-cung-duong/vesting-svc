package com.edso.nft.common;

public class Constant {
    public final static String USER_NAME = "USER_NAME";
    public final static String WALLET_ADDRESS = "WALLET_ADDRESS";
    public final static String LANGUAGE = "LANGUAGE";

    public final static String EN = "en";
    public final static String VI = "VI";
    public final static String ZH = "ZH";

    public final static String TRACE_ID = "traceId";
    public final static String TIME_REQUEST = "time-request";
}
