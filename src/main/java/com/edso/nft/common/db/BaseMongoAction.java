package com.edso.nft.common.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseMongoAction {
    protected final Logger logger = LoggerFactory.getLogger(this.getClass());
}

