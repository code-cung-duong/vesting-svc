package com.edso.nft.common.info;

import lombok.Data;

@Data
public class HeaderInfo {
    private String userId;
    private String username;
    private String walletAddress;
    private Integer language;
    private Integer roles;
}
