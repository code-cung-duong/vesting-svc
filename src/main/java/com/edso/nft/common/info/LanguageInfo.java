package com.edso.nft.common.info;

import lombok.Data;

@Data
public class LanguageInfo {
    private int code;
    private String en;
    private String vi;
    private String zh;
}
