package com.edso.nft.common;

public class DBConfigKey {
    public static final String TXN_HASH = "txn_hash";
    public static final String MONGO_ID = "_id";
    public static final String IS_HAS_TOKEN = "is_has_token";
    public static final String AMOUNT = "amount";
    public static final String SCHEME = "scheme";
    public static final String WALLET_ADDRESS = "wallet_address";
    public static final String STATUS = "status";
    public static final String CREATED_AT = "created_at";
    public static final String UPDATED_AT = "updated_at";
    public static final String IS_DELETE = "is_delete";
    public static final String TOTAL_AMOUNT_VESTING = "total_amount_vesting";
    public static final String CURRENT_AMOUNT_VESTING = "current_amount_vesting";
    public static final String CLAIMED_TOTAL_AMOUNT = "claimed_total_amount";
    public static final String STARTED_TIME = "started_time";
    public static final String ENDED_TIME = "ended_time";
    public static final String SCHEME_NAME = "scheme_name";
    public static final String CRYPTO_ASSET_SYMBOL = "crypto_asset_symbol";
    public static final String DURATION_TIME = "duration_time";
    public static final String AFTER_START_TIME = "after_start_time";
    public static final String AFTER_START_TYPE = "after_start_type";
    public static final String PERIOD = "period";
    public static final String PERCENT_TOKEN_RELEASE = "percent_token_release";
    public static final String IS_LOCKED = "is_locked";
    public static final String SCHEME_ID = "scheme_id";
}
