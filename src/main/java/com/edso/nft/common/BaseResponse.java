package com.edso.nft.common;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.apache.logging.log4j.ThreadContext;

@Data
public class BaseResponse {
    protected int responseCode;
    protected String message;

    @JsonProperty("trace-id")
    private String traceId;

    public void setSuccess() {
        this.responseCode = 200;
        this.message = "OK";
        this.injectTraceId();
    }

    public void setFailed(int rc, String rd) {
        this.responseCode = rc;
        this.message = rd;
        this.injectTraceId();
    }

    private void injectTraceId() {
        this.traceId = ThreadContext.get(Constant.TRACE_ID);
    }

    public String info() {
        return "rc=" + responseCode + "|rd=" + message;
    }

}
