package com.edso.nft.common;

public class CollectionNameDefs {
    public static final String COLL_VESTING = "coll_vesting";
    public static String COLL_VESTING_SCHEME_PLAN = "coll_vesting_scheme_plan";
    public static final String COLL_SCHEME = "coll_scheme";
    public static final String COLL_SCHEME_PLAN = "coll_scheme_plan";
}
