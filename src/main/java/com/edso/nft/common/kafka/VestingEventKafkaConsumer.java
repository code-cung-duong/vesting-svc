package com.edso.nft.common.kafka;

import com.edso.nft.service.BaseService;
import com.edso.nft.service.impl.VestingKafkaProcess;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class VestingEventKafkaConsumer extends BaseService {
    private final VestingKafkaProcess process;

    public VestingEventKafkaConsumer(VestingKafkaProcess process) {
        this.process = process;
    }

    @KafkaListener(topics = "${kafka.topic-vesting}",
            groupId = "${kafka.group-vesting}",
            containerFactory = "vestingKafkaListenerContainerFactory")
    public void receiveMessage(@Payload String event) throws Exception {
        logger.info("=>receiveKafkaMessage {}", event);
        process.process(event);
    }
}
