package com.edso.nft.controller;


import com.edso.nft.common.GetArrayResponse;
import com.edso.nft.dto.response.VestingResponseDTO;
import com.edso.nft.service.VestingService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/v1.0.0/vestings")
@Slf4j
@RequiredArgsConstructor
public class VestingController {
    private final VestingService vestingService;

    @GetMapping()
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page", dataType = "integer", paramType = "query",
                    value = "Results page you want to retrieve (0..N)", defaultValue = "0"),
            @ApiImplicitParam(name = "size", dataType = "integer", paramType = "query",
                    value = "Number of records per page.", defaultValue = "15"),
            @ApiImplicitParam(name = "sort", allowMultiple = true, dataType = "string",
                    paramType = "query", value = "Sorting criteria in the format: property(,asc|desc). "
                    + "Default sort order is ascending. Multiple sort criteria are supported.")})
    public GetArrayResponse<VestingResponseDTO> getListVesting(
            HttpServletRequest request,
            @RequestParam(value = "walletAddress", required = false) String walletAddress,
            @RequestParam(value = "symbolToken", required = false) String symbolToken,
            @RequestParam(value = "status", required = false) Integer status,
            @ApiIgnore Pageable pageable
            ) {
        return vestingService.getListVesting(request,walletAddress,symbolToken,status,pageable);
    }
//
//    @GetMapping("/{id}")
//    public ResponseEntity<VestingResponseDTO> getDetailVesting(
//            HttpServletRequest request,
//            @PathVariable Long id
//    ) {
//        return ResponseEntity.ok(vestingService.getDetail(request,id));
//    }
//
//    @PostMapping("create")
//    public ResponseEntity<VestingCreateResponseDTO> createVesting(
//            HttpServletRequest request,
//            @Valid @RequestBody VestingCreateRequestDTO requestDTO
//    ) {
//        return ResponseEntity.ok(vestingService.createVesting(request, requestDTO));
//    }
//
//    @PutMapping("active/{id}")
//    public ResponseEntity<VestingResponseDTO> createVesting(
//            HttpServletRequest request,
//            @PathVariable Long id
//    ) {
//        return ResponseEntity.ok(vestingService.activeVesting(request, id));
//    }
}
