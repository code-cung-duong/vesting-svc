package com.edso.nft.enumtype;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum VestingStatus {

    INACTIVE(0),
    ACTIVE(1),
    COMPLETED(2),
    PROCESSING_TRANSACTION(3);

    @Getter
    private final Integer code;

    public static VestingStatus get(Integer id) {
        return Objects.requireNonNull(Arrays.stream(VestingStatus.values())
                .filter(s -> s.getCode().equals(id))
                .findFirst()
                .orElse(null));
    }
}
