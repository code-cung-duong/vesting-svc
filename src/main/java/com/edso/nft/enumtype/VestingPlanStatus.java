package com.edso.nft.enumtype;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.Objects;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum VestingPlanStatus {

    CLIFF(0),
    RELEASED(1),
    LOCK(2),
    CLAIMED(3);

    @Getter
    private final Integer code;

    public static VestingPlanStatus get(Integer id) {
        return Objects.requireNonNull(Arrays.stream(VestingPlanStatus.values())
                .filter(s -> s.getCode().equals(id))
                .findFirst()
                .orElse(null));
    }
}
