# defi-nft-inventory

Common for Inventory service, ...

# Hướng dẫn build module Inventory service

# Tech Stack
## Core
- Maven 3.8.3
- Java 8 (1.8.0_202)
- Spring boot 2.1.1

# Build and install
## 1. Build common lib
```shell
git clone https://gitlab.com/edsolabs/edsolabs-defi/microservice/backend/nft-common.git
cd nft-common
git checkout tags/<deployment-tag>
mvn install
```

## 2. Build project
```shell
git clone https://gitlab.com/edsolabs/edsolabs-defi/microservice/backend/inventory-svc.git
cd inventory-svc
git checkout tags/<deployment-tag>
mvn install
```
## 3. Copy các file và thư mục vào 1 chung 1 thư mục:
- config/
- lib/
- inventory-svc-1.0.jar
```shell
mkdir -p /opt/defi-backend/15-inventory-svc
cp -R config/ ~/home/app/15-inventory-svc/config
cp -R lib/ ~/home/app/15-inventory-svc/lib
cp inventory-svc-1.0.jar ~/home/app/nft-api-svc/inventory-svc-1.0.jar
```
## 4. Cấu hình file
1. Cấu hình log4j.xml
```text
- Cấu hình đường dẫn log
- Cấu hình tên file log
```
2. Cấu hình application.yml
```text
- Cấu hình thông tin database (market_db và nft_db)
- Cấu hình eureka server url
```
# Run
```shell
cd /opt/defi-backend/15-inventory-svc
java -Xmx512m -Xms256m -Dfile.encoding=UTF8 -Dspring.config.location=config/application.yml -jar inventory-svc-1.0.jar
```

